---
title: "Disclaimer"
description: "Disclaimer Blog Liu Purnomo"
date: 2022-04-05T21:06:30+07:00

---
Berikut Dibawah Adalah Syarat Dan Ketentuan Untuk Pengguna situs kami, Liu Purnomo (http://www.liupurnomo.com) yang wajib anda patuhi.

## Pengguna Situs Kami:

1. Liu Purnomo sebagai pemilik dari sebuah situs ini.
2. Pengguna atau Pengunjung situs ini dengan kata lain setuju dengan syarat dan ketentuan yang kami buat, yang dapat berubah sewaktu-waktu tanpa ada pemberitahuan.
3. Penggunaan dan pengunjung Situs ini menunjukkan persetujuan Anda terhadap Syarat dan Ketentuan yang kami buat, karena anda menggunakan situs ini.

## Syarat Ketentuan Layanan:

Syarat dan ketentuan layanan berikut bersifat mengikat bagi semua pengunjung dan pengguna situs. Kami sebagai penyedia Bg Io Blog ini bertanggung jawab dalam pengelolaan publikasi artikel, perubahan konten, pengaturan kunjungan dan diskusi. Dengan menggunakan atau mengunjungi situs ini berarti Anda setuju dengan syarat dan ketentuan berikut:

1. Anda tidak akan melakukan pembajakan/plagiat konten baik dari situs ini keluar atau pun dari luar ke dalam situs ini dalam ranah ketentuan atau aturan mana pun Dan dengan ini Anda tunduk pada ketentuan DMCA ( Digital Millennium Copyright Act Notice ).
2. Anda tidak akan mempublikasikan materi yang berada dalam hak kekayaan intelektual tanpa izin ke dalam blog ini.
3. Anda tidak akan melakukan spam baik secara acak atau sengaja dalam berbagai bentuk di blog ini.
4. Anda tidak akan memberikan pranala dan/atau taut yang bertujuan untuk meningkatkan/mengalihkan trafik ke suatu situs komersial tertentu, atau dengan sengaja melakukan penipuan dalam pranala terselubung (phising, proofing, dan lain sebagainya).
5. Anda tidak akan melakukan penetrasi/hack ke dalam sistem basis data, termasuk menanamkan peranti lunak berbahaya (worm, virus, trojan) yang merusak konten dan/atau membahayakan pengguna lainnya.
6. Anda tidak akan menerbitkan atau memberikan pranala/taut yang merupakan konten pornografi, tidak senonoh, tidak sopan, baik dalam bentuk teks, gambar atau pun video.
7. Anda akan menggunakan sistem diskusi secara sopan dan etis, dan tidak melakukan penyerangan, penghinaan, dan ketidaklayakan yang lainnya terhadap pengguna yang lain.
8. Anda tidak akan memasukkan URL Website/URL blog atau situs lain yang memberikan pesan elektronik tak diinginkan, atau metode-metode promosi serupa yang tidak sehat. Dan kami tidak bertanggung jawab, atas apa yang anda lakukan.

## Pelanggaran Yang Akan Terjadi Jika Melanggar Syarat Dan Ketentuan Layanan Diatas:

Jika terjadi pelanggaran terhadap syarat dan ketentuan, maka kami (pemilik Situs) berhak melakukan berbagai tindakan yang dinilai perlu seperti penghapusan konten/kontribusi/diskusi yang tidak sesuai, hingga penghapusan akun kontributor/subscriber, penolakan untuk berpartisipasi kembali, Hingga Melaporkannya pada Pihak yang bersangkutan.
Demikian Syarat dan Ketentuan ini kami buat agar dapat dipergunakan sebagaimana mestinya.

Terakhir Kali di Update pada Maret 2018
