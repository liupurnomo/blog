---
categories : Drone
title: "Pilihan Sensor Pada Trinity F90+"
image: daftar-pilihan-sensor-pada-trinity.webp
date: 2022-04-04T22:54:49+07:00
description : "Trinity F90+ Quantum Systems adalah drone buatan Quantum Systems GmbH. Drone eVTOL adalah drone yang sangat di rekomenadiskan untuk pemetaan."
---
Trinity F90+ Quantum Systems adalah drone buatan Quantum Systems GmbH. Drone eVTOL adalah drone yang sangat di rekomenadiskan untuk pemetaan.

Salah satu alasannya adalah banyaknya pilihan sensor pada drone ini.

Dan hebatnya, sensor – sensor tersebut di buat khusus untuk drone ini, sehingga sangat kompatibel untuk drone trinity f90+ dan Aplikasi Mission Planner QBase.

Apa saja pilihan sensor pada Trinity F90+ ini? berikut di bawah ini daftar sensor – sensornya.

## Sony UMC-R10C Payload

![Sony UMC-R10C Payload!](/img/sony-umc-r10c-payload.webp "Sony UM-R10C Payload")

20.1 Megapixel RGB Sensor untuk Trinity F90+ UAV

* Orthophoto & DEM
* point clouds & 3D models
* 2.66cm/px & 2.1cm/px @ 100m altitude
* 16 mm & 20 mm lens option

Kamera Sony UMC-R10C 20.1MP digunakan untuk melakukan pemetaan menggunakan drone. Kamera ini sangat ideal untuk menangkap gambar berkualitas tinggi untuk membuat peta atau model 3D.

Ini dapat dilakukan bahkan dalam kondisi cahaya rendah berkat sensor gambar APS-C yang besar. Rana global memungkinkan kita untuk menangkap gambar tanpa membuat efek rana bergulir yang tidak diinginkan.

Berkat faktor bentuk kecil DARI UMC R10C, itu dapat dikombinasikan dengan sensor lain dalam drone dengan sistem double payload. Ini memungkinkan Anda untuk mengumpulkan lebih banyak data dalam waktu yang lebih singkat.

## Sony RX1R II Payload

42.4 Megapixel RGB Sensor for the Trinity F90+ UAV

* Orthophoto & DEM
* point clouds & 3D models
* 1.29cm/px @ 100m altitude


![Sony RX100 Payload! class="img-fluid"](/img/sony-rx-100.webp "Sony RX100")

Berkat sensor CMOS 35 mm dengan 42,4 megapiksel, Sony RX1RII sangat ideal untuk pemetaan yang membutuhkan hasil resolusi tinggi. Terutama di bidang DEM & orthophoto serta awan titik resolusi tinggi dan model 3D terperinci sangat mudah dengan gambar yang diambil oleh Sony RX1RII.

Anda bisa menghasilkan GSD dalam kisaran 1,29cm/px pada ketinggian terbang 100m!

## Qube 240 LiDAR

![Qube 240 Lidar!](/img/lidar-qube-240.webp "Qube 240 Lidar")

QUBE 240 adalah salah satu pilihan payload LIDAR dengan standard “Geomatics Grade” untuk melengkapi opsi payload Trinity F90+ Quantum Systems. Telah terbukti mampu mengudara melebihi 60 menit dari kapasitas cadangan baterai tersisa 30 menit untuk melakukan pendaratan dan kondisi darurat.

Qube 240 LiDAR merupakan penerus dari LiDAR Surveyor Ultra yang telah lebih dahulu digunakan pada TRON Quantum Systems. Pada tahun 2020, Quantum Systems memutuskan untuk menghentikan produksi TRON, dan fokus mengembangkan Trinity.

Akhirnya muncullnya Qube240 ini, LiDAR yang cukup ringan sehingga bisa di angkut oleh Trinity F90+ Quantum Systems.

## MicaSense Altum Payload

![MicaSense Altum Payload!](/img/micasense-altum.webp "MicaSense Altum Payload")

5 spectral bands & thermal infrared sensor

* RGB data with 4.31cm/px @100m altitude
* multispectral data + thermal layer for enhanced analysis
* 1 flight => orthophoto (6bands) & DEM & point cloud
* various indices for plant stress and health analysis

ALTUM Mengintegrasikan kamera termal radiometrik dengan five high-resolution narrow bands , menghasilkan citra termal, multispektral, dan resolusi tinggi canggih dalam satu penerbangan untuk advanced analytics.

completely pre-assembled payload, ready to fly
easily accessible memory card from the outside
The Calibrated Reflectance Panel and DLS2 sensor are available separately.
Altum membutuhkan light sensor (DLS2) . Sensor ini harus dipasang di bagian atas bodi utama UAV. Setiap TrinityF90Plus dapat dipasang kembali dengan senor ini oleh pelanggan. Versi yang dimodifikasi dapat dipesan secara terpisah.

## MicaSense RedEdge-MX Payload

![MicaSense RedEdge-MX Payload!](/img/micasense-rededge-mx.webp "MicaSense RedEdge-MX Payload")

5 spectral band sensor

* RGB data with 6.94cm/px @100m altitude
* multispectral data for vegetation analysis
* 1 flight => orthophoto (5bands) & DEM & point cloud
* various indices for plant stress and health analysis

The RedEdge-MX is a rugged, built-to-last, professional multispectral sensor. It captures the spectral bands required for basic crop health indexes and additional bands needed for advanced analytics.

* Completely pre-assembled payload, ready to fly
* Easily accessible memory card from the outside
* The Calibrated Reflectance Panel and DLS2 sensor are available separately.

The RedEdge-MX requires a light sensor (DLS2). This sensor has to be mounted on the top of the main body of the UAV. Each TrinityF90Plus can be retrofitted with this senor by the customer. A modified version can be ordered separately.