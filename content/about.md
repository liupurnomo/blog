---
title: "About Me"
description: ""
date: 2022-04-05T10:08:04+07:00

---

Jangan salah sangka, meskipun nama Saya Liu, saya bukan orang China, Bapak Dayak, Mamak Melayu, lahir di Kalimantan, Punya nama Liu Purnomo, agak unik memang, namun karena Saya anak yang berbakti, saya tidak pernah protes.

Dayak itu suku ya, begitupun melayu, meskipun saya Dayak, karena bapak dayak, Saya Muslim. Dulu Bapak, waktu menikah dengan Almarhum Mamak, Masuk islam, Mualaf. Nenek dari Bapak, Katolik, waktu natal, kami sering natalan ke tempat Nenek.

Saya sudah menikah, Istri saya Jawa, dari Banyumas. Alhamdulillah, kami di karunia Puta Putri yang menggemaskan. Saya dan Istri tinggal di Jakarta, Punya KTP DKI, ikut serta dalam pemilihan Gubernur DKI, meski tidak pernah tertarik dengan pembahasan soal Politik, Apalagi kalau sudah bahas soal Kampret Cebong, saya skip.

Kalau ditanya soal pekerjaan, Saya seorang Frelance, mengerjakan banyak hal, terutama yang berkaitan dengan Drone. Saya memegan beberapa sertifikat, seperti sertikat Remote Pilot dari DKUPPU, dan sertifikat sebagai Master Instruktur dari BNSP.

Selain kesibukan bekerja, Saat ini, Saya juga sedang melanjutkan pendidikan di STMIK Antar Bangsa, Mengambil S1 Teknik Informatika, dan Institut Daarul Qur’an mengambil S1 Ilmu Al-Qur’an dan Tafsir.